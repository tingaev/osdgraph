#!/usr/bin/env python
import json

def lists(data):
    return ' '.join(['"{0}"'.format(c) for c in data])

def lists_idx(data):
    return ' '.join(['"'+idx[c]+'"' for c in data])


with open('ceph.json') as json_file:
    data = json.load(json_file)

idx = {}
dd = {}
dd['root-names'] = []
dd['host-names'] = []
dd['osd-names'] = []
#index
for nodes in data['nodes']:
  idx[nodes['id']] = nodes['name']

#print
for nodes in data['nodes']:
  if (nodes['type'] == 'root'): 
      dd['root-names'].append(nodes['name'])
      dd['root-children',nodes['name']] = lists_idx(nodes['children'])
  if (nodes['type'] == 'host'): 
      dd['host-names'].append(nodes['name'])
      dd['host-children',nodes['name']] = lists_idx(nodes['children'])
  if (nodes['type'] == 'osd'): 
      dd['osd-names'].append(nodes['name'])
#      dd['osd-children'] = lists_idx(nodes['children'])


print('digraph dot {\n' 'graph [layout = neato]\n' 'edge [color = grey]\n' 'node [shape = circle, style = filled, color = grey]\n')
print('node [fillcolor = blue] ',lists(dd['root-names']))
print('node [fillcolor = green] ',lists(dd['host-names']))
print('node [fillcolor = orange] ',lists(dd['osd-names']))
for c in dd['root-names']:
  print('"{0}"'.format(c),' -> {', dd['root-children',c],'}')
for c in dd['host-names']:
  print('"{0}"'.format(c),' -> {', dd['host-children',c],'}')

print('}')


